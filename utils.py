"""Utils functions for VSE IO.
"""
import os
from pathlib import Path
from string import digits
from typing import Union


def get_audio_extensions() -> frozenset:
    """Get all file extensions that should be considered as audio.

    Extracted from Blender source code at https://developer.blender.org/diffusion/B/browse/master/source/blender/imbuf/intern/util.c%2495
    """
    return frozenset(
        [
            ".wav",
            ".ogg",
            ".oga",
            ".mp3",
            ".mp2",
            ".ac3",
            ".aac",
            ".flac",
            ".wma",
            ".eac3",
            ".aif",
            ".aiff",
            ".m4a",
            ".mka",
        ]
    )


def get_video_extensions() -> frozenset:
    """Get all file extensions that should be considered as video.

    Extracted from Blender source code at https://developer.blender.org/diffusion/B/browse/master/source/blender/imbuf/intern/util.c%2495
    """
    return frozenset(
        [
            ".avi",
            ".mpg",
            ".mpeg",
            ".dvd",
            ".vob",
            ".mp4",
            ".mov",
            ".dv",
            ".ogg",
            ".ogv",
            ".mkv",
            ".flv",
            ".webm",
        ]
    )


def get_image_extensions() -> frozenset:
    """Get all file extensions that should be considered as image.

    Extracted from Blender source code at https://developer.blender.org/diffusion/B/browse/master/source/blender/imbuf/intern/util.c%2495
    """
    return frozenset(
        [
            ".bmp",
            ".cin",
            ".dds",
            ".dpx",
            ".exr",
            ".hdr",
            ".j2c",
            ".jp2",
            ".jpeg",
            ".jpg",
            ".pdd",
            ".png",
            ".psb",
            ".psd",
            ".rgb",
            ".rgba",
            ".sgi",
            ".tga",
            ".tif",
            ".tiff",
            ".tx",
        ]
    )
