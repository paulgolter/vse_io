# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name": "VSE IO",
    "author": "Félix David",
    "description": "Import and export editing files into Blender's Sequencer using the OpenTimelineIO python module.",
    "blender": (2, 93, 0),
    "version": (0, 2, 4),
    "location": "File > Import-Export",
    "warning": "",
    "category": "Import-Export",
}
from os.path import expandvars
from pathlib import Path
import re
import json
import subprocess
from typing import List, Union
import copy

import opentimelineio as otio
from opentimelineio.opentime import RationalTime, TimeRange
from opentimelineio.schema import (
    Clip,
    ExternalReference,
    Gap,
    Stack,
    Timeline,
    Track,
    ImageSequenceReference
)
from vse_io import metadata


import bpy
from bpy.types import Operator, MovieSequence, SoundSequence, MetaSequence

# ImportHelper is a helper class, defines filename and
# invoke() function which calls the file selector.
from bpy_extras.io_utils import ExportHelper, ImportHelper
from bpy.props import StringProperty, EnumProperty, FloatProperty
from bpy.types import Operator
from operator import attrgetter

from .utils import (
    get_audio_extensions,
    get_image_extensions,
    get_video_extensions,
)

extensions = {
    "audio": get_audio_extensions(),
    "image": get_image_extensions(),
    "video": get_video_extensions(),
}
volume_max = 1 # Maximum volume of all strips, will be calculated automatically.
PREMIERE_MAX = 3.98109

def get_nmbr_of_audio_channels(file_path: Path) -> int:
    # Should be exposed by Blender Python API
    # this method requires ffmpeg to be installed
    # and increases export time considerably.
    cmd = [
        "ffprobe",
        "-v",
        "quiet",
        "-print_format",
        "json",
        "-show_format",
        "-show_streams",
        file_path.as_posix(),
    ]
    output = subprocess.check_output(cmd)
    obj = json.loads(output)
    for stream in obj["streams"]:
        if stream["codec_type"] == "audio":
            return int(stream["channels"])
    return 0


def get_strip_filepath(strip: bpy.types.Sequence) -> Path:
    if strip.type == "MOVIE":
        filepath = Path(strip.filepath)
    elif strip.type == "SOUND":
        filepath = Path(strip.sound.filepath)
    elif strip.type == "IMAGE":
        image = strip.elements[0]
        filepath = Path(strip.directory, image.filename)

    if filepath.as_posix().startswith("//"):
        filepath = Path(filepath.as_posix()[2:])


    return filepath

def calc_volume_max(context):
    global volume_max
    volume_max = 1
    volumes = [
        strip.volume
        for strip in context.scene.sequence_editor.sequences_all
        if strip.type == "SOUND"
    ]
    if volumes:
        volume_max = max(volumes)

# ============== Export ================


class SEQUENCER_OT_export_editing(Operator, ExportHelper):
    """Export VSE editing to file using OpenTimelineIO"""

    bl_idname = "sequencer.export_editing"
    bl_label = "Export Editing"

    filter_glob: StringProperty(
        default=f"*.{';*.'.join(otio.adapters.suffixes_with_defined_adapters(write=True))}",
        options={"HIDDEN"},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )

    filename_ext: EnumProperty(  # TODO waiting for this https://developer.blender.org/T85688
        name="Timeline format",
        description="Choose timeline format",
        items=[
            (f".{a}", a, a)
            for a in otio.adapters.suffixes_with_defined_adapters(write=True)
        ],
        default=".otio",
    )

    def execute(self, context):
        scene = context.scene
        sequence_editor = context.scene.sequence_editor

        # Build timeline
        # ==============
        timeline = Timeline(scene.name)

        # Add timeline metadata
        metadata.add_metadata_to_timeline(timeline, context)

        # Calculate audio level range
        calc_volume_max(context)

        # Build sequences tree
        self.append_sequences_tree_into_stack(
            sequence_editor.sequences,
            timeline.tracks,
            track_frame_start=scene.frame_start,
        )
        # ==============

        # TODO use timeline.to_json_file() when 0.14.0 is supported everywhere

        # Customize export for specific formats
        kwargs = {}

        # AAF
        if Path(self.filepath).suffix == ".aaf":
            kwargs["use_empty_mob_ids"] = True

        # Export
        otio.adapters.write_to_file(timeline, self.filepath, **kwargs)

        return {"FINISHED"}

    def append_sequences_tree_into_stack(
        self,
        sequences: List[Union[MetaSequence, MovieSequence, SoundSequence]],
        main_stack: Stack,
        track_frame_start=0,
    ):
        """Append sequences tree into Stack object.

        Creates a new Stack for every meta sequence.

        :param sequences: Sequences to build the Stack from
        :param main_stack: Main Stack to append sequences tree in
        :param track_frame_start: First frame of track beginning, used with nested meta for Stacks
        """
        global volume_max # Is calculated before this function is called
        global PREMIERE_MAX
        scene = bpy.context.scene
        scene_fps = scene.render.fps / scene.render.fps_base
        audio_factor = PREMIERE_MAX / volume_max

        created_sequences_count = 0
        for channel_index in range(1, 128):

            # Get all Sequences for current cannel.
            track_sequences = sorted(
                [
                    s
                    for s in sequences
                    if s.channel == channel_index
                    and s.type in ["META", "MOVIE", "SOUND", "IMAGE"]
                ],
                key=attrgetter("frame_final_start"),
            )

            # Skip if channel is empty.
            if len(track_sequences) == 0:
                continue

            # Create new track TODO manage scene.tracks names
            track = Track(f"Track {channel_index}")
            track.kind = "Audio"  # Audio by default to be changed is any other type is present in the track
            track.metadata.update(copy.deepcopy(metadata.xml_track))
            main_stack.append(track)

            last_bound = scene.frame_start
            for seq in track_sequences:
                # Create gap
                frames_gap = seq.frame_final_start - last_bound
                if frames_gap > 0:
                    gap = Gap()
                    gap.source_range = TimeRange(
                        RationalTime(0, scene_fps),
                        RationalTime(frames_gap, scene_fps),
                    )

                    track.append(gap)

                # If metastrip call this function again
                if seq.type == "META":
                    clip = Stack(seq.name)
                    self.append_sequences_tree_into_stack(
                        seq.sequences, clip, track_frame_start=seq.frame_final_start
                    )
                else:
                    # Create clip
                    filepath = get_strip_filepath(seq)
                    clip = Clip(seq.name)
                    ext_ref = ExternalReference(filepath.as_posix())
                    ext_ref.name = filepath.name
                    clip.media_reference = ext_ref

                    if seq.type == "SOUND":
                        # Add audio levels xml metadata

                        clip.metadata.update(copy.deepcopy(metadata.xml_audio_level))
                        clip.metadata["fcp_xml"]["filter"]["effect"]["parameter"]["value"] = (seq.volume * audio_factor)

                        # Set stereo / mono metadata
                        if Path(bpy.path.abspath(seq.sound.filepath)).exists():
                            nmbr_of_channels = get_nmbr_of_audio_channels(Path(bpy.path.abspath(seq.sound.filepath)))
                            if nmbr_of_channels < 2:
                                clip.metadata["fcp_xml"]["@premiereChannelType"] = "mono"
                            else:
                                clip.metadata["fcp_xml"]["@premiereChannelType"] = "stereo"

                            # Set mediareference metadata
                            clip.media_reference.metadata.setdefault("fcp_xml", {"media": {"audio": {}}})
                            clip.media_reference.metadata["fcp_xml"]["media"]["audio"]["channelcount"] = nmbr_of_channels

                    if seq.type == "IMAGE":
                        elements = seq.elements
                        if len(elements) > 1:  # Image Sequence

                            # Determine zero padding
                            zero_padding = len(
                                re.search(
                                    f"\d+?(?=\{filepath.suffix})",
                                    filepath.name,
                                )[0]
                            )

                            # Overwrite media_reference to be ImageSequenceReference
                            clip.media_reference = ImageSequenceReference(
                                name_suffix=filepath.suffix,
                                frame_zero_padding=zero_padding,
                                target_url_base=filepath.parent.as_posix(),
                            )
                            clip.media_reference.name = filepath.name

                # Set Time
                # --------
                # Set source range
                clip.source_range = TimeRange(
                    RationalTime(seq.frame_offset_start, scene_fps),
                    RationalTime(seq.frame_final_duration, scene_fps),
                )

                # Set available range
                if seq.type != "META":
                    clip.media_reference.available_range = TimeRange(
                        RationalTime(0, scene_fps),
                        RationalTime(seq.frame_duration, scene_fps),
                    )

                # Determine Track kind
                if seq.type != "SOUND":
                    track.kind = "Video"

                # Add clip to track
                track.append(clip)

                # Set last bound
                last_bound = seq.frame_final_end

                # Avoid out of RAM
                if created_sequences_count >= 85:
                    bpy.ops.sequencer.refresh_all()
                    created_sequences_count = 0
                else:
                    created_sequences_count += 1


# Only needed if you want to add into a dynamic menu
def menu_func_export(self, context):
    self.layout.operator(
        SEQUENCER_OT_export_editing.bl_idname,
        text=f"Editing (.{', .'.join(otio.adapters.suffixes_with_defined_adapters(write=True))})",
    )


# ============== Import ================
class SEQUENCER_OT_import_editing(Operator, ImportHelper):
    """Import editing from file using OpenTimelineIO"""

    bl_idname = "sequencer.import_editing"
    bl_label = "Import Editing"

    # ImportHelper mixin class uses this
    filename_ext = ".otio"

    filter_glob: StringProperty(
        default=f"*.{';*.'.join(otio.adapters.suffixes_with_defined_adapters(read=True))}",
        options={"HIDDEN"},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )

    def execute(self, context):
        scene = context.scene
        sequence_editor = scene.sequence_editor

        # Read timeline file
        timeline = otio.adapters.read_from_file(self.filepath)

        # Set scene frame start and end
        scene.frame_start = (
            0 if timeline.global_start_time is None else timeline.global_start_time
        )
        scene.frame_end = otio.opentime.to_frames(timeline.duration()) - 1
        # TODO otio.opentime.to_frames() must be replaced by timeline.duration().to_frames() when retrocompatibility
        # won't be necessary any more, let's say 1 year after Blender 3.0 and OTIO 0.14.0 release.

        # Build tracks
        for i, track in enumerate(timeline.tracks, 1):
            self.build_composable(
                sequence_editor.sequences,
                track,
                frame_offset=scene.frame_start,
                channel_index=i,
            )

            # Add track
            scene.tracks.add().name = track.name or f"Track {i}"

        return {"FINISHED"}

    def build_composable(
        self,
        sequences_stack: List[Union[MetaSequence, MovieSequence, SoundSequence]],
        composable: Union[Stack, Track],
        channel_index=1,
        frame_offset=1,
    ) -> List[Union[MetaSequence, MovieSequence, SoundSequence]]:
        """Build composable (Stack, Track).

        Creates the sequences based on composable.range_in_parent().

        :param sequences_stack: Current sequences stack (a.k.a meta) to create given composable into
        :param composable: Composable to build
        :param channel_index: Channel to build composable, defaults to 1
        :param frame_start: First frame of clip's sequence, defaults to 1
        :return: Created sequences
        """
        created_sequences = []
        created_sequences_count = 0

        # Set channel offset
        channel_offset = 0

        for child in composable.each_child(shallow_search=True):
            # Set frame start
            child_range_in_parent = child.range_in_parent()
            frame_start = frame_offset + otio.opentime.to_frames(
                child_range_in_parent.start_time
            )

            # Set channel
            channel_index = channel_index + channel_offset

            if type(child) is Stack:  # Build Stack
                meta = sequences_stack.new_meta(
                    child.name,
                    channel_index,
                    frame_start,
                )

                # Build into VSE
                sequences = self.build_composable(
                    meta.sequences,
                    child,
                    channel_index=channel_index,
                    frame_offset=frame_start,
                )

                # Apply stack attributes to meta
                self.set_data(child, meta)

                # Update sequences to return
                sequences = [meta]

            elif type(child) is Track:  # Build Track
                sequences = self.build_composable(
                    sequences_stack,
                    child,
                    channel_index=channel_index,
                    frame_offset=frame_start,
                )

                channel_offset += 1
            elif type(child) is Clip:  # Build Clip
                sequences = [
                    self.build_clip(
                        sequences_stack,
                        child,
                        composable.kind,
                        channel_index=channel_index,
                        frame_start=frame_start,
                    )
                ]

                # Avoid out of RAM
                if created_sequences_count >= 85:
                    bpy.ops.sequencer.refresh_all()
                    created_sequences_count = 0
                else:
                    created_sequences_count += 1

            else:
                sequences = []

            # Keep new sequences
            created_sequences.extend([seq for seq in sequences if seq])

        return created_sequences

    def build_clip(
        self,
        sequences_stack: bpy.types.SequencesMeta,
        clip: Clip,
        kind: str,
        channel_index=1,
        frame_start=1,
    ) -> Union[MetaSequence, MovieSequence, SoundSequence]:
        """Create clip into a sequence.

        Allows to force sound loading from movie file:
        ``if track.kind == Audio`` all movie files will be loaded as sound sequences.

        For image sequences, place holder are used by default when frames are missing.

        :param sequences_stack:
        :param clip: Clip to create into VSE
        :param kind: Way to load the media: Video or Audio
        :param channel_index: Channel to create the clip's sequence to, defaults to 1
        :param frame_start: First frame of clip's sequence, defaults to 1
        :return: Created sequence
        """
        media_reference = clip.media_reference

        # Image sequence
        if type(media_reference) is ImageSequenceReference:
            sequence_directory = Path(expandvars(media_reference.target_url_base))

            # Get image files
            image_files = sorted(
                sequence_directory.glob(
                    f"{media_reference.name_prefix}*{media_reference.name_suffix}"
                )
            )

            # Create images sequence
            sequence = sequences_stack.new_image(
                name=clip.name,
                filepath=str(image_files[0]),
                channel=channel_index,
                frame_start=frame_start,
            )

            # Add images of sequence
            frame = media_reference.start_frame + 1
            for image in image_files[1:]:
                # Get current image number
                current_file_number = int(
                    re.search(
                        f"(?<={media_reference.name_prefix})(\d+?)(?={media_reference.name_suffix})",
                        image.name,
                    )[0]
                )

                # Add missing frames
                while frame < current_file_number:
                    missing_image_name = "".join(
                        [
                            media_reference.name_prefix,
                            str(frame).zfill(media_reference.frame_zero_padding),
                            media_reference.name_suffix,
                        ]
                    )
                    sequence.elements.append(missing_image_name)

                    # Increment current frame
                    frame += 1

                # Append the current image
                sequence.elements.append(image.name)

                # Increment current frame
                frame += 1

        # Any other media reference
        else:
            if type(media_reference).__name__ == "UnknownSchema":
                self.report(
                    {"ERROR"},
                    f"Installed version of OpenTimelineIO doesn't handle the schema of {clip}. Please upgrade it.",
                )
                return

            media_path = Path(expandvars(media_reference.target_url))

            # Is it defined to be a sound or actually a sound file
            if kind == "Audio" or media_path.suffix.lower() in extensions["audio"]:
                sequence = sequences_stack.new_sound(
                    name=clip.name,
                    filepath=str(media_path),
                    channel=channel_index,
                    frame_start=frame_start,
                )

            # Fallback on guessing if image or movie
            else:
                if media_path.suffix.lower() in extensions["image"]:  # Image
                    sequence = sequences_stack.new_image(
                        name=clip.name,
                        filepath=str(media_path),
                        channel=channel_index,
                        frame_start=frame_start,
                    )
                elif media_path.suffix.lower() in extensions["video"]:  # Movie
                    sequence = sequences_stack.new_movie(
                        name=clip.name,
                        filepath=str(media_path),
                        channel=channel_index,
                        frame_start=frame_start,
                    )

        # Apply clip attributes to sequence
        self.set_data(clip, sequence)

        return sequence

    @staticmethod
    def set_data(
        element: Union[Stack, Clip],
        sequence: Union[MetaSequence, MovieSequence, SoundSequence],
    ):
        """Set data from element to sequence.

        :param element: OpenTimelineIO Element
        :param sequence: Blender sequence
        """
        # Offset
        trimmed_range = element.trimmed_range()
        frame_offset_start = otio.opentime.to_frames(trimmed_range.start_time)
        # Test for optimize, don't modify VSE when not needed
        if sequence.frame_offset_start != frame_offset_start:
            sequence.frame_offset_start = frame_offset_start

        # Duration
        duration = otio.opentime.to_frames(trimmed_range.duration)
        # Test for optimize, don't modify VSE when not needed
        if sequence.frame_final_duration != duration:
            sequence.frame_final_duration = duration

        # Data
        name = element.name
        if name:
            sequence.name = name

        # Metadata
        for key, value in element.metadata.items():
            sequence[key] = value


# Only needed if you want to add into a dynamic menu
def menu_func_import(self, context):
    self.layout.operator(
        SEQUENCER_OT_import_editing.bl_idname,
        text=f"Editing (.{', .'.join(otio.adapters.suffixes_with_defined_adapters(read=True))})",
    )


# This allows you to right click on a button and link to documentation
# def add_object_manual_map():
#     url_manual_prefix = "https://docs.blender.org/manual/en/latest/"
#     url_manual_mapping = (
#         ("bpy.ops.mesh.add_object", "scene_layout/object/types.html"),
#     )
#     return url_manual_prefix, url_manual_mapping
class VSEIO_Track(bpy.types.PropertyGroup):
    """Store Track data from imported timeline.

    Mostly here to be able to access fulfilled data from imported editing file.
    Not meant to be handled during editing.
    """

    name: bpy.props.StringProperty(name="Track Name")


classes = [
    SEQUENCER_OT_export_editing,
    SEQUENCER_OT_import_editing,
    VSEIO_Track,
]


def register():

    for cls in classes:
        bpy.utils.register_class(cls)

    # Store tracks names
    bpy.types.Scene.tracks = bpy.props.CollectionProperty(type=VSEIO_Track)

    # Export
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

    # Import
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)
    # bpy.utils.register_manual_map(add_object_manual_map)


def unregister():

    for cls in classes:
        bpy.utils.unregister_class(cls)

    del bpy.types.Scene.tracks

    # Export
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)

    # Import
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)
    # bpy.utils.unregister_manual_map(add_object_manual_map)


if __name__ == "__main__":
    register()
