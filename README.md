# Blender VSE IO

![VSE IO UI](images/VSE-IO_Blender.jpg)

:warning:**Currently in alpha testing!**
**Feedbacks are more than welcome**

This addon imports and exports editing files into Blender's Sequencer using the [OpenTimelineIO](https://opentimelineio.readthedocs.io/en/latest/index.html) python module.

### Supported formats
- Read: ale, fcpxml, edl, xml, m3u8, otiod, otio, mb, aaf, ma, kdenlive, xges, otioz
- Write: ale, rv, fcpxml, edl, xml, m3u8, otiod, otio, mb, aaf, ma, kdenlive, xges, otioz

```mermaid
graph LR;
    B((Blender))

    ale-->B;
    fcpxml-->B;
    edl-->B;
    xml-->B;
    m3u8-->B;
    otiod-->B;
    otio-->B;
    otioz-->B;
    mb-->B;
    aaf-->B;
    ma-->B;
    kdenlive-->B;
    xges-->B;

    B-->ale_o[ale];
    B-->fcpxml_o[fcpxml];
    B-->edl_o[edl];
    B-->xml_o[xml];
    B-->m3u8_o[m3u8];
    B-->otiod_o[otiod];
    B-->otio_o[otio];
    B-->otioz_o[otioz];
    B-->mb_o[mb];
    B-->aaf_o[aaf];
    B-->ma_o[ma];
    B-->kdenlive_o[kdenlive];
    B-->xges_o[xges];
    B-->rv;
     
    style B fill:#0000,stroke:#0000
    linkStyle default interpolate basis
```

**Works only from [Blender 2.93](https://builder.blender.org/download/).**

## How to
First of all, you must install the `opentimelineio` module into Blender's python.

### Setup
#### Win
1. Open your Blender version folder
1. Go to `2.93 > python > bin`
1. In the URL bar, click and type `cmd`
1. Run `python.exe -m pip install git+https://github.com/PixarAnimationStudios/OpenTimelineIO.git`

#### Mac
1. Right click on your Blender package
1. "Show Package Contents"
1. Go to `Ressources > 2.93 > python`
1. Right click on `bin` folder
1. Open terminal window to the folder
1. Run `python3.7m -m pip install opentimelineio`

#### Linux
1. Open your Blender version folder
1. Go to `2.93 > python > bin`
1. Open a terminal to this directory
1. Run `python3.7m -m pip install opentimelineio`

#### OpenTimelineIO doesn't work
Due to python incompatibilities between some OTIO and some Python's Blender versions under Blender 3.0 and OpenTimelineIO 0.14.0, you may want to `pip install opentimelineio==0.11.0`, which was the last fully pythonic version of OTIO. It lacks management of images sequences, but may still be used with movies.

### Installation
Install it as an usual addon:
1. Download the code as a `zip`
1. Open Blender
1. Open the `Preferences`
1. Go to `Add-ons`
1. Click on `Install...`
1. Find the add-on on your disk
1. Enable it
1. Enjoy

### Usage
#### Import
You can find the import operator in `File > Import > Editing`.

#### Export
You can find the export operator in `File > Export > Editing`.

##### Settings
- Timeline format: You can choose the format to export your editing.
    
    **Known issue: the extension of the picked format is appended instead of being modified.
    That's because of: https://developer.blender.org/T85688.
    Let's see how it goes**

### Features
#### Environment variables in paths
Adding environment variables in [external references](path.to.otio) paths is allowed:

```python
clip.media_reference = otio.schema.ExternalReference(target_url="$PROJECT_ROOT/rel/path/to/foo.mov")
clip.media_reference = otio.schema.ExternalReference(target_url="${PROJECT_ROOT}/rel/path/to/foo.mov")
```

#### Footage handles
Using OpenTimelineIO you can define if the current version has frame handles at start and end: offset by a certain number of frames in the beginning and stripped at the end. Default is 0 frame handles, the whole media is used.

`Clip.source_range = TimeRange`

```python
clip = otio.schema.Clip()
clip.source_range = otio.opentime.range_from_start_end_time(
    otio.opentime.RationalTime(10, 25),
    otio.opentime.RationalTime(media_length - 10, 25)
)
```

### Contribute
You can contribute by proposing MRs on `master` or by opening issues.

Add-on sponsored by Superprod Studio