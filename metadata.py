xml_effect_audio_level = {
    'fcp_xml': {
        'effectcategory': 'audiolevels',
        'effectid': 'audiolevels',
        'effecttype': 'audiolevels',
        'mediatype': 'audio',
        'parameter': {
            '@authoringApp': 'PremierePro',
            'name': 'Level',
            'parameterid': 'level',
            'value': '1',
            'valuemax': '3.98109',
            'valuemin': '0'
            },
        'pproBypass': 'false'
        }
}
xml_audio_level = {
    "fcp_xml": {
            "@premiereChannelType": "stereo",
            "filter": {
                "effect": {
                    "effectcategory": "audiolevels",
                    "effectid": "audiolevels",
                    "effecttype": "audiolevels",
                    "mediatype": "audio",
                    "name": "Audio Levels",
                    "parameter": {
                        "@authoringApp": "PremierePro",
                        "name": "Level",
                        "parameterid": "level",
                        "value": "1",
                        "valuemax": "3.98109",
                        "valuemin": "0"
                    },
                    "pproBypass": "false"
                }
            }
    }
}

xml_track = {
    "fcp_xml": {
        "@currentExplodedTrackIndex": "0",
        "@premiereTrackType": "Stereo",
        "@totalExplodedTrackCount": "2",
        "enabled": "TRUE"
        }
}

xml_timeline = {
    "fcp_xml": {
        "media": {
            "video": {
                "format": {
                    "samplecharacteristics": {
                        "anamorphic": "FALSE",
                        "fielddominance": "none",
                        "height": "856",
                        "pixelaspectratio": "square",
                        "rate": {"ntsc": "FALSE", "timebase": "24"},
                        "width": "2048",
                    }
                }
            },
        },
        "rate": {"ntsc": "FALSE", "timebase": "24"},
    }
}

xml_mediaref = {
    "fcp_xml": {
        "media": {
            "audio": {
                "channelcount": 2,
            }
        }
    }
}

def add_metadata_to_timeline(timeline, context):
    global xml_timeline

    timeline.metadata.update(xml_timeline)
    timeline.metadata["fcp_xml"]["media"]["video"]["format"]["samplecharacteristics"]["height"] = context.scene.render.resolution_y
    timeline.metadata["fcp_xml"]["media"]["video"]["format"]["samplecharacteristics"]["width"] = context.scene.render.resolution_x
    timeline.metadata["fcp_xml"]["media"]["video"]["format"]["samplecharacteristics"]["rate"]["timebase"] = context.scene.render.fps
    timeline.metadata["fcp_xml"]["rate"]["timebase"] = context.scene.render.fps



"""
# Add audio levels effect, not strictly necessary, for premiere
# clip.metadata['fcp_xml'] seems to be enough
effect = Effect()
effect.name = 'Audio Levels'
effect.metadata.update(copy.deepcopy(metadata.xml_effect_audio_level))
effect.metadata["fcp_xml"]["parameter"]["value"] = (seq.volume * audio_factor)
clip.effects.append(effect)
"""
